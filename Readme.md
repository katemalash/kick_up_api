install docker, docker-compose, knex

clone repository https://katemalash@bitbucket.org/katemalash/kick_up_api.git

1. install dependencies 'npm install'
2. start db containers 'docker-compose up'
3. add migrations 'knex migrate:latest'
4. fill dbs 'knex seed:run'
5. start server 'npm start'

server starts at http://localhost:3001
